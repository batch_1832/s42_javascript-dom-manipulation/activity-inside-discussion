// console.log("Hello WorldS")
// [SECTION] Document Object manipulation
// it allows us to access or modify the properties of an element in a webpage.
// it is a standard on how to get, change, add or delete HTML elements.
/*
	Syntax: document.querySelector("htmlElement");

		- The query selector function takes a string input that is formatted like a CSS Selector when applying styles.
*/

// alternative way on retrieving HTML Elwmwnts
// document.getElementById("#text-first-name");
// document.getElementByclass()

// However, using these functions requires us to identify beforehand we gat the element. With querySelector, we can be flexible in how to retrieve elements.

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
const colors = document.querySelector("#txt-colors");

// [SECTION] Event Listeners
// When a user interacts with the webpage, this action is considered as an event. (Example: mouse click, mouse hover, page load, key press etc...
/*
// addEventListener takes two arguments:
	// a string that identify an event.
	// a function that a listener will execute once a "specified event" is triggered.
txtFirstName.addEventListener("keyup", () =>{
	// ".inner HTML" property sets or return the HTML content(inner HTML) of an element (div, spans, etc.)
	//".value" property sets or returns the value of an attribute (form controls).
	spanFullName.innerHTML = `${txtFirstName.value}`;
});*/
/*
// When the event occurs, an "event object" is passed to the function argument as the first parameter
txtFirstName.addEventListener("keyup", (event) =>{
	// the "event.target" contains the element where the event happened
	console.log(event.target);
	// the "event.target.value" gets the value of the input object(txt-first-name)
	console.log(event.target.value);
});
*/
// Creating Multiple event that use the same function.
const fullName = () =>{
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
}
txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);


//Activity

/*
Create another addEventListener that will "change the color" of the "spanFullName". Add a "select tag" element with the options/values of red, green, and blue in your index.html.

Check the following links to solve this activity:
	HTML DOM Events: https://www.w3schools.com/jsref/dom_obj_event.asp
	HTML DOM Style Object: https://www.w3schools.com/jsref/dom_obj_style.asp

*/
colors.addEventListener("change", (event)=>{
	if(event.target.value == "red"){
		spanFullName.style.color = "red";
	}
	else if(event.target.value == "green"){
		spanFullName.style.color = "green";
	}
	else if(event.target.value == "blue"){
		spanFullName.style.color = "blue";
	}
	
})